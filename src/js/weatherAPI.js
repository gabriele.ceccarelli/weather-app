import data from './../../weather-config.json'
import { getWeekday } from './utility'  

import { sliderInit } from './slider.js'
import { controllerInit } from './controller.js'

export function weatherInit(){
    
    var weekday = [];
    weekday[0] = "sun";
    weekday[1] = "mon";
    weekday[2] = "tue";
    weekday[3] = "wed";
    weekday[4] = "thu";
    weekday[5] = "fri";
    weekday[6] = "sat";

    var cityGroup = '';
    var pagination = '';
    var cities = data.cities;

    pagination += '<div class="widget__pagination">';
    for(var i = 0; i < cities.length; i++){
        $('.widget__slider').append('<div class="widget__slide card card--empty" data-city="'+ cities[i].key +'" style="background-image: url('+cities[i].image+')"></div>');

        pagination += '<div class="widget__bullet" data-pagination="'+ cities[i].key +'"></div>';

        cityGroup += cities[i].key;
        if(i < cities.length - 1)
            cityGroup += ',';
    }
    pagination += '</div>';
    $('.widget__bar').append(pagination);

    $('.widget__bullet').on('click', function(){
        var value = $(this).attr('data-pagination');
        window.goToSlide(null, value);
    })

    if(cityGroup){
        var url = getGroupURL(cityGroup)
        $.get( url, function(data) {
            if(data && data.list.length){
                var list = data.list;
                
                for(var i = 0; i < list.length; i++){
                    var item = list[i];
                    let id = item.id;
                    var city = item.name;
                    var lon = item.coord.lon;
                    var lat = item.coord.lat;
                    var description = item.weather[0].main;
                    var temp = item.main.temp && Math.floor(item.main.temp);
                    var tempMin = item.main.temp_min && Math.floor(item.main.temp_min);
                    var tempMax = item.main.temp_max && Math.floor(item.main.temp_max);
                    $('.widget__slide[data-city="'+item.id+'"]').append(printSlide(id, city, description, temp, tempMin, tempMax));
                    $('.widget__slide[data-city="'+item.id+'"]').removeClass('card--empty');

                    var _dot = $('.widget__slide[data-city="'+id+'"] .card__forecast').append('<div class="dot"></div>')

                    var forecastUrl = getForecastUrl(lat, lon);
                    $.get( forecastUrl, function(data) {
                        if(data && data.daily.length){
                            var forecast = data.daily;
                            $('.widget__slide[data-city="'+id+'"] .card__forecast').append(printForecast(forecast));
                            $('.widget__slide[data-city="'+id+'"] .card__forecast .dot').remove();
                        }
                    })
                    .fail(function(){
                        $('.widget__slide[data-city="'+id+'"] .card__forecast').append('<p>Not available</p>');
                        $('.widget__slide[data-city="'+id+'"] .card__forecast .dot').remove();
                    })
                }


                $('.widget__slide.card--empty').each(function(){
                    printError($(this).attr('data-city'));
                })

            }else
                printError();
            
        })
        .fail(function(){
            printError()
        })
        .always(function() {
            sliderInit()
            controllerInit()
        });
    }
    

    function getGroupURL(group){
        if(group){
            return 'https://api.openweathermap.org/data/2.5/group?id='+group+'&units=metric&appid='+process.env.API_ROOT
        }
    }

    function getForecastUrl(lat, lon){
        if(lat && lon)
            return 'https://api.openweathermap.org/data/2.5/onecall?lat='+lat+'&lon='+lon+'&units=metric&appid='+process.env.API_ROOT
    }

    function getIconUrl(code){
        if(code)
            return 'https://openweathermap.org/img/wn/'+code+'@2x.png';
    }

    function printSlide(key, name, description, temp, tempMin, tempMax){
        var slide = '';
        if(key){
            slide += '<div class="card__content">';
            slide += '<div class="card__main">';
            slide += '<div class="card__headline">';
            slide += '<h2>'+ name +'</h2>';
            slide += '<p>'+ description +'</p>';
            slide += '</div>';
            slide += '<div class="card__temperature">';
            slide += '<p class="card__temp extra-large-text">'+printCelsius(temp)+'</p>';
            slide += '<p class="large-text">'+printCelsius(tempMin)+'/'+printCelsius(tempMax)+'</p>';
            slide += '</div>';
            slide += '</div>';
            slide += '<div class="card__forecast"></div>';
            slide += '</div>';
        }
        return slide;
    }

    function printForecast(forecast){
        var output= '';

        if(forecast.length){
            output += '<div class="card__daily">';
            for(var i = 1; i < forecast.length; i++){
                //skip the first item
                var date = forecast[i].dt && new Date(forecast[i].dt*1000);
                var day = getWeekday(date.getDay());
                var tempMin = forecast[i].temp.min && Math.floor(forecast[i].temp.min);
                var tempMax = forecast[i].temp.max && Math.floor(forecast[i].temp.max);
                var desc = getIconUrl(forecast[i].weather[0].main);
                var icon = getIconUrl(forecast[i].weather[0].icon);
                output += '<div class="card__daily-item">';
                output += '<p class="large-text">'+day+'</p>';
                output += '<img src="'+icon+'" alt="'+desc+'"/>';
                output += '<small>'+printCelsius(tempMin)+'/'+printCelsius(tempMax)+'</small>';
                output += '</div>';
            }
            output += '</div>';
        }

        return output;
    }

    function printError(id){
        if(typeof id !== 'undefined'){
            $('.widget__slide[data-city="'+id+'"]').append('<div class="widget__error">Ops, something went wrong</div>');
            return
        }
        $('.widget__slide').append('<div class="widget__error">Ops, something went wrong</div>');
    }

    function printCelsius(value){
        return value + '&deg;';
    }
}