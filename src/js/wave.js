export function waveInit(){

    if(typeof DeviceOrientationEvent === 'undefined')
        $('.widget__switch').hide()

    $('.widget__switch input').on('change', function(){
        var check = $(this).prop('checked')
        $('.widget__wave-background').addClass('animated');

        if(check){
            if (typeof DeviceOrientationEvent.requestPermission === 'function') {
                DeviceOrientationEvent.requestPermission()
                    .then(function(state){
                        if(state === 'granted'){
                            window.addEventListener('deviceorientation', handleOrientation);
                        }else{
                            console.error('Request to access the orientation was rejected');
                        }
                    })
                    .catch(function(){
                        $('.widget__switch').hide()
                    });
            } else {
                window.addEventListener('deviceorientation', handleOrientation);
            }
        }else{
            window.removeEventListener('deviceorientation', handleOrientation)
        }
    })


    function handleOrientation(event){
        var gamma = event.gamma/10;
        window.requestAnimationFrame(function(){
            var translateSmall = 50 + gamma;
            var translateBig = 50 - gamma;
            $('.widget__wave-background.small').css('transform', 'translateX(-'+translateSmall+'%)')
            $('.widget__wave-background.big').css('transform', 'translateX(-'+translateBig+'%)')
        });
    }
}