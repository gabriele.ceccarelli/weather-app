import data from './../../weather-config.json'

export function sliderInit(){

    var currentSlide = 0;
    var cities = data.cities;
    var xCoordinate = null;
    var isListActive = false

    var widget = $('.widget');
    var widgetWidth = window.matchMedia("(max-width: 768px)").matches ? $(window).width() : widget.width();
    var widgetSlider = $('.widget__slider');
    var widgetSlides = widgetSlider.children('.widget__slide');
    widgetSlides.css('width', widgetWidth);
    widgetSlides.css('flex-basis', 'auto');
    widgetSlider.css('width', widgetWidth * widgetSlides.length)
    widget.attr('data-current-city', cities[0].key)
    $('[data-pagination='+ cities[0].key +']').addClass('active')

    $(document).on('mousedown touchstart', lock);
    $(document).on('mouseup touchend', move);
    $(document).on('touchmove', function(e){ e.preventDefault() });

    $(window).resize(function(){
        widgetWidth = widget.width();
        widgetSlides.css('width', widgetWidth);
        widgetSlider.css('width', widgetWidth * widgetSlides.length)
    })

    $('.widget__list').on('click', function(){
        $('.widget').toggleClass('widget--list')
        isListActive = !isListActive;
    })

    $('.widget__slide').on('click', function(){
        if(isListActive){
            goToSlide(null, $(this).attr('data-city'))
            $('.widget').removeClass('widget--list')
            isListActive = false;
        }
    })

    window.goToNext = function(){
        if(!isListActive && currentSlide < widgetSlides.length - 1)
            goToSlide(currentSlide + 1)
    }

    window.goToPrev = function(){
        if(!isListActive && currentSlide > 0)
            goToSlide(currentSlide - 1)
    }

    window.goToSlide = function(index, slide){
        if(index == null){
            for(var i = 0; i < cities.length; i++){
                if( cities[i].key == slide )
                    index = i
            }
        }
        currentSlide = index;
        var currentCity = slide || cities[currentSlide].key;
        widgetSlider.css('transform', 'translate(calc('+index+'/'+widgetSlides.length+'*-100%))');

        widget.attr('data-current-city', currentCity)
        $('[data-pagination]').removeClass('active')
        $('[data-pagination='+ currentCity +']').addClass('active')
    }

    function getClientX(e){
        if(e){
            if(e.changedTouches){
                return  e.changedTouches[0].clientX;
            }
            return e.clientX;
        }
    }

    function lock(e){
        if(!isListActive)
        xCoordinate = getClientX(e);
    }

    function move(e){
        if(!isListActive && (xCoordinate || xCoordinate === 0)) {
            var dx = getClientX(e) - xCoordinate;
            var direction = Math.sign(dx);

            if( (currentSlide > 0 || direction < 0) &&  (currentSlide < widgetSlides.length - 1 || direction > 0) ){
                goToSlide(currentSlide -= direction)
            }
            
            xCoordinate = null
        }
    }

}