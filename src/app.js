import './main.scss';

window.$ = require('jquery');
import { weatherInit } from './js/weatherAPI.js'
import { waveInit } from './js/wave.js'

$(function(){
    weatherInit()
    waveInit()
});
    
    